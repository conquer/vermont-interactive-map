
    // Set window height + width
    d3.selection.prototype.moveToFront = function() {
      return this.each(function(){
        this.parentNode.appendChild(this);
      });
    };

    var width = 4000,
        height = 1200;

    // Define map projection
    var projection = d3.geo.transverseMercator()
        .rotate([72.57, -44.20])
        .translate([400,400])
        .scale([20000]);

    // Define path generator
    var path = d3.geo.path()
        .projection(projection);

    // Create SVG Element
    var svg = d3.select('body').append('svg')
        .attr('width', width)
        .attr('height', height);



    // Load icons
    var iconPreaching, iconGospel, iconAll;

    d3.xml("Preaching-01.svg", "image/svg+xml", function(error, documentFragment) {
      if (error) {console.log(error); return;}

       var svgNode = documentFragment.getElementsByTagName("svg")[0];
       svgNode.id = 'preachingicon';

       svg.node().appendChild(svgNode);
       var innerSVG = svg.select("#preachingicon");

       iconPreaching = innerSVG;

       iconPreaching.attr('visibility', 'hidden');
    });
    d3.xml("Evangelism-01.svg", "image/svg+xml", function(error, documentFragment) {
      if (error) {console.log(error); return;}

       var svgNode = documentFragment.getElementsByTagName("svg")[0];
       svgNode.id = 'gospelicon';
       svg.node().appendChild(svgNode);
       var innerSVG = svg.select("#gospelicon");
       innerSVG.attr()
       iconGospel = innerSVG;

       iconGospel.attr('visibility', 'hidden');
    });
    // d3.xml("FullInfo-01.svg", "image/svg+xml", function(error, documentFragment) {
    //   if (error) {console.log(error); return;}
    //
    //    var svgNode = documentFragment.getElementsByTagName("svg")[0];
    //    svgNode.id = 'fullinfoicon'
    //
    //    svg.node().appendChild(svgNode);
    //    var innerSVG = svg.select("#fullinfoicon");
    //
    //    iconAll = innerSVG;
    //
    //    iconAll.attr('visibility', 'hidden');
    // });

    // Define scale to sort data values into color buckets
    var color;

    // Load CSV
    d3.csv('town-data.csv', function(data) {

      var numArray = data.map(function(d) {
        return d.contacts;
      });

      var min = Math.min.apply(null, numArray);
      var max = Math.max.apply(null, numArray);

      color = d3.scale.linear().domain([min, max]).range(["#ddd","#1A2C4C"]);

        // Load TopoJSON
        d3.json('vermont.json', function(error, vt) {

            for (var i = 0; i < data.length; i++) {
                var dataTown = data[i].town;
                var dataPop = parseFloat(data[i].contacts);

                for (var j = 0; j < vt.objects.vt_towns.geometries.length; j++) {
                    var jsonTown = vt.objects.vt_towns.geometries[j].properties.town;

                    if (dataTown.toUpperCase() == jsonTown) {
                        vt.objects.vt_towns.geometries[j].properties.contacts = dataPop;
                        break;
                    }
                }
            }

            var vermont = topojson.feature(vt, vt.objects.vt_towns);

            svg.append('path')
                .datum(vermont)
                .attr('d', path)
                .style('stroke', '#777')
                .style('stroke-width', '1');


            // var g = svg.append('g')
            //     .attr('class', 'key')
            //     .attr('transform', 'translate(320, 165)')
            //     .call(yAxis);
            //
            // g.selectAll('rect')
            //     .data(color.range().map(function(d, i) {
            //         return {
            //             y0: i ? y(color.domain()[i - 1]) : y.range()[0],
            //             y1: i < color.domain().length ? y(color.domain()[i]) : y.range()[1],
            //             z: d
            //         };
            //     }))
            //     .enter().append('rect')
            //         .attr('width', 8)
            //         .attr('y', function(d) { return d.y0; })
            //         .attr('height', function(d) { return d.y1 - d.y0; })
            //         .style('fill', function(d) { return d.z; });





            svg.selectAll('.subunit')
                .data(topojson.feature(vt, vt.objects.vt_towns).features)
            .enter().append('path')
                .attr('d', path)
                .style('fill', function(d) {
                    var contacts = d.properties.contacts;
                    if (d.properties.town == "BENNINGTON") {
                      return 'darkgray';
                    } else {
                      if (contacts) {
                          return color(contacts);
                      } else {
                          return '#ddd';
                      }
                    }

                })


                .on('mouseover', function(d) {
                    if (d.properties.town == "BENNINGTON") { return; }

                    var xPosition = d3.mouse(this)[0];
                    var yPosition = d3.mouse(this)[1] - 30;



                    var g  = svg.append('g').attr("id", "town");
                    var box = svg.append('g').attr("id", "infobox");

                    box.append('text')
                    .attr("id", "infoboxtowntitle")
                    .attr('text-anchor', 'middle')
                    .attr("x", 810)
                    .attr("y", 300)
                    .attr('font-family', '"PT Sans", sans-serif')
                    .attr('font-size', '24px')
                    .attr('font-weight', 'bold')
                    .attr('text-transform', 'uppercase')
                    .attr('fill', '#1A2C4C')
                    .text(d.properties.town);

                    box.append('text')
                    .attr("id", "vermonttext")
                    .attr('text-anchor', 'middle')
                    .attr("x", 810)
                    .attr("y", 325)
                    .attr('font-family', '"PT Sans", sans-serif')
                    .attr('font-size', '20px')
                    .attr('font-weight', 'bold')
                    .attr('text-transform', 'uppercase')
                    .attr('fill', 'darkgray')
                    .text("Vermont");

                    var sermonstext = box.append('text')
                    .attr("id", "sermonspreached")
                    .attr('text-anchor', 'left')
                    .attr("x", 700)
                    .attr("y", 390)
                    .attr('font-family', '"PT Sans", sans-serif')
                    .attr('font-size', '22px')
                    .attr('font-weight', 'bold')
                    .attr('text-transform', 'uppercase')
                    .style('fill', 'white')
                    .text(d.properties.contacts + " Sermons Preached");

                    g.append("rect")
                    .attr("id", "sermonstextback")
                    .attr("rx", 6)
                    .attr("ry", 6)
                    .attr("x", sermonstext.node().getBBox().x - 10)
                    .attr("y", sermonstext.node().getBBox().y - 10)
                    .attr("width", 280)
                    .attr("height", sermonstext.node().getBBox().height + 20)
                    .style("fill", '#537C56');

                    d3.select("#sermonspreached").moveToFront();

                    iconPreaching.attr('visibility', 'visible')
                    .attr("width", 48)
                    .attr("height", 48)
                    .attr("x", 630)
                    .attr("y", 390 -32)
                    .style('fill', '#537C56');

                    var gospeltext = box.append('text')
                    .attr("id", "gospelpresentations")
                    .attr('text-anchor', 'left')
                    .attr("x", 700)
                    .attr("y", 460)
                    .attr('font-family', '"PT Sans", sans-serif')
                    .attr('font-size', '22px')
                    .attr('font-weight', 'bold')
                    .attr('text-transform', 'uppercase')
                    .style('fill', 'white')
                    .text(d.properties.contacts + " Gospel Presentations");

                    g.append("rect")
                    .attr("id", "gospelpresentationsback")
                    .attr("rx", 6)
                    .attr("ry", 6)
                    .attr("x", gospeltext.node().getBBox().x - 10)
                    .attr("y", gospeltext.node().getBBox().y - 10)
                    .attr("width", 280)
                    .attr("height", gospeltext.node().getBBox().height + 20)
                    .style("fill", '#1A2C4C');

                    d3.select("#gospelpresentations").moveToFront();

                    iconGospel.attr('visibility', 'visible')
                    .attr("width", 48)
                    .attr("height", 48)
                    .attr("x", 630)
                    .attr("y", 460 -32)
                    .style('fill', '#1A2C4C');

                    var totaltext = box.append('text')
                    .attr("id", "totalcontacts")
                    .attr('text-anchor', 'left')
                    .attr("x", 700)
                    .attr("y", 530)
                    .attr('font-family', '"PT Sans", sans-serif')
                    .attr('font-size', '22px')
                    .attr('font-weight', 'bold')
                    .attr('text-transform', 'uppercase')
                    .style('fill', 'white')
                    .text(d.properties.contacts + " Total Gospel Contacts");

                    g.append("rect")
                    .attr("id", "totalcontactsback")
                    .attr("rx", 6)
                    .attr("ry", 6)
                    .attr("x", totaltext.node().getBBox().x - 10)
                    .attr("y", totaltext.node().getBBox().y - 10)
                    .attr("width", 280)
                    .attr("height", totaltext.node().getBBox().height + 20)
                    .style("fill", '#96999B');

                    d3.select("#totalcontacts").moveToFront();

                    // iconAll.attr('visibility', 'visible')
                    // .attr("width", 48)
                    // .attr("height", 48)
                    // .attr("x", 630)
                    // .attr("y", 530 -32)

                    var text = g.append('text')
                    .attr("id", "townname")
                    .attr('text-anchor', 'middle')
                    .attr("x", xPosition)
                    .attr("y", yPosition)
                    .attr('font-family', '"PT Sans", sans-serif')
                    .attr('font-size', '11px')
                    .attr('font-weight', 'bold')
                    .attr('text-transform', 'uppercase')
                    .attr('fill', '#1A2C4C')
                    .text(d.properties.town);

                    g.append("rect")
                    .attr("id", "townnameback")
                    .attr("rx", 6)
                    .attr("ry", 6)
                    .attr("x", text.node().getBBox().x - 10)
                    .attr("y", text.node().getBBox().y - 10)
                    .attr("width", text.node().getBBox().width + 20)
                    .attr("height", text.node().getBBox().height + 20)
                    .style("fill", 'white');

                    d3.select("#townname").moveToFront();

                    d3.select(this)
                    .style('fill', '#987434');

                })
                .on('mouseout', function(d) {
                  if (d.properties.town == "BENNINGTON") { return; }
                  
                  d3.select('#town').remove();
                  d3.select('#infobox').remove();

                  // iconAll.attr('visibility', 'hidden')
                  iconPreaching.attr('visibility', 'hidden')
                  iconGospel.attr('visibility', 'hidden')

                  d3.select(this)
                  .transition()
                  .duration(250)
                  .style('fill', function(d) {

                  var contacts = d.properties.contacts;

                  if (contacts) {
                      return color(contacts);
                  } else {
                      return '#ddd';
                  }
                });



            });





            // svg.append('path')
            //     .datum(topojson.feature(vt, vt.objects.lake))
            //     .attr('d', path)
            //     .style('stroke', '#89b6ef')
            //     .style('stroke-width', '1px')
            //     .style('fill', '#b6d2f5');

        });
    });
